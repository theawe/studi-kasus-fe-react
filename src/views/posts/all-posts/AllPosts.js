import React from 'react'
import {
    CTable,
    CTableHead,
    CTableRow,
    CTableHeaderCell,
    CTableBody,
    CTableDataCell,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilPencil, cilTrash } from '@coreui/icons'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'

export default function AllPosts() {
    const url = 'http://localhost:8080/'
    const navigate = useNavigate()
    const [posts, setPosts] = useState([])

    function getAllPostByPage() {
        axios.get(`${url}article/5/0`).then((response) => {
            console.log(response.data)
            setPosts(response.data)
        })
    }

    useEffect(() => {
        getAllPostByPage()
    }, [])

    function navigateToEditPost() {
        navigate('/posts/edit')
    }

    return (
        <CTable>
            <CTableHead color="dark">
                <CTableRow>
                    <CTableHeaderCell scope="col">Title</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Category</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                </CTableRow>
            </CTableHead>
            <CTableBody>
                {posts.map((post) => {
                    return (
                        <CTableRow>
                            <CTableDataCell>{post.title}</CTableDataCell>
                            <CTableDataCell>{post.category}</CTableDataCell>
                            <CTableDataCell>
                                <button onClick={navigateToEditPost}>
                                    <CIcon icon={cilPencil} size="xl" />
                                </button>
                                <button>
                                    <CIcon icon={cilTrash} size="xl" />
                                </button>
                            </CTableDataCell>
                        </CTableRow>
                    )
                })}
            </CTableBody>
        </CTable>
    )
}
